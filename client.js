(function () {
	var RTCPeerConnection = webkitRTCPeerConnection;
	var getUserMedia = navigator.webkitGetUserMedia.bind(navigator);

	var peer;
	var peerConfiguration = {'iceServers': [ {'url': 'stun: stun.l.google.com:19302' }]};

	var signalingChannel;
	var callToken;

	var isCallee;

	// callerとcalleeの判定をlocation.hashで行う
	if (document.location.hash === "" || document.location.hash === undefined) {
		// you are the Caller
		callToken = 1;
		document.location.hash = callToken;
		document.title = "Caller";
		document.getElementById("loading_state").innerHTML = "Ready for a call...ask your friend to visit:<br/><br/>"+document.location;
	} else {
		// you are the Callee
		isCallee = true;
		callToken = document.location.hash.substr(1);
		document.title = "Callee";
		document.getElementById("loading_state").innerHTML = "One moment please...connecting your call...";
	}

	// 接続開始
	start();

	/*
	 * P2Pネットワークへの接続を開始
	 */
	function start() {
		peer = new RTCPeerConnection(peerConfiguration);

		// ローカルサイドの「IPとポートの組み合わせ」候補が見つかると呼ばれる
		// singalingChannelを介してリモートへ送信する
		peer.onicecandidate = function (event) {
			if (event.candidate) {
				send({token: callToken, type: 'new_ice_candidate', candidate: event.candidate});
			}
		};

		// ローカルのpeerへaddStreamされると呼ばれる
		// ネットワークへ参加することをsingalingChannelへ通知する
		// 自分がcalleeの場合はsingalingChannelを介してリモートへ通知する
		peer.onnegotiationneeded = function (event) {
			// signalingChannelの接続先はWebサーバと同じとする
			signalingChannel = new WebSocket('ws://' + location.host);
			signalingChannel.onopen = function () {
				send({token: callToken, type: 'join'});
				if (isCallee) {
					send({token: callToken, type: 'callee_arrived'});
				}
			}
			signalingChannel.onmessage = receive;
		};

		// ローカルのpeerにリモートのsdp(offerもしくはanswer)をsetRemoteDescriptionすると呼ばれる
		// つまりリモートのstreamをとれる
		peer.onaddstream = function (event) {
			document.getElementById("remote_video").src = URL.createObjectURL(event.stream);
			document.getElementById("loading_state").style.display = "none";
			document.getElementById("open_call_state").style.display = "block";
		};

		// ユーザーにメディアへのアクセス許可をとる
		// 許可がとれたらpeerにstreamを設定する
		getUserMedia(
			{'audio': true, 'video': true},
			function (stream) {
				peer.addStream(stream);
				document.getElementById("local_video").src = URL.createObjectURL(stream);
			},
			logError
		);
	}

	/*
	 * SignalingChannelへメッセージを送信する
	 */
	function send(obj) {
		signalingChannel.send(JSON.stringify(obj));
	}

	/*
	 * SignalingChannelからメッセージを受信し、メッセージタイプごとに処理する
	 */
	function receive(event) {
		var signal = JSON.parse(event.data);
		if (signal.type === "callee_arrived") {
			// callerは、calleeの到着が通知されたらcalleeへofferを送る
			createOffer();
		} else if (signal.type === "new_ice_candidate") {
			// リモートから「IPとポートの組み合わせ」候補を受信したらローカルのpeerに登録する
			addIceCandidate(signal.candidate);
		} else if (signal.type === 'new_offer') {
			// calleeは、callerからofferを受信したらpeerに設定してcallarへanswerを送る
			setRemoteDescription(signal.offer);
			createAnswer();
		} else if (signal.type === "new_answer") {
			// callerは、calleeからanswerを受信したらpeerに設定する
			setRemoteDescription(signal.answer);
		} else {
			console.log('unknown type');
		}

		function createOffer() {
			peer.createOffer(
				function (offer) {
					peer.setLocalDescription(offer);
					send({token: callToken, type: 'new_offer', offer: offer});
				},
				logError
			);
		}

		function createAnswer() {
			peer.createAnswer(
				function (answer) {
					peer.setLocalDescription(answer);
					send({token: callToken, type: 'new_answer', answer: answer});
				},
				logError
			);
		}

		function addIceCandidate(candidate) {
			peer.addIceCandidate(new RTCIceCandidate(candidate));
		}

		function setRemoteDescription(sdp) {
			peer.setRemoteDescription(
				new RTCSessionDescription(sdp),
				function () {},
				logError
			);
		}
	}

	/*
	 * 汎用的なエラーログ出力関数
	 */
	function logError(error) {
		console.log(error);
	}

}());
